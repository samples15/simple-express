const express = require('express');

const router = express.Router();
const axios = require('axios');
//
const rateLimit = require('express-rate-limit');
const slowDown = require('express-slow-down');
// API
const BASE_URL = 'https://api.nasa.gov/insight_weather/?';

// Caching
let cachedData;
let cacheTime;

// Hide Keywith a custom one
const apiKeys = new Map();
apiKeys.set('12345', true);

const rateLimiter = rateLimit({
  windowMs: 30 * 1000, // 30 Sec
  max: 10 // limit each IP to 3 requests per windowMs
});

const speedLimiter = slowDown({
  windowMs: 30 * 1000, // 15 minutes
  delayAfter: 1, // allow 1 requests per 3sec, then...
  delayMs: 500 // begin adding 500ms of delay per request above 1:
});

router.get('/', rateLimiter, speedLimiter, (req, res, next) => {
  // Middleware
  const apiKey = req.get('X-API-KEY');
  if (apiKeys.has(apiKey)) {
    next();
  } else {
    const error = new Error('Invalid Api Key');
    next(error);
  }
}, async (req, res, next) => {
  // In memory Cache
  if (cacheTime && cacheTime > Date.now() - 30 * 1000) {
    return res.json(cachedData);
  }
  try {
    const params = new URLSearchParams({
      api_key: process.env.NASA_API_KEY,
      feedtype: 'json',
      version: 1.0,
    });
    // Make request to NASA Api
    const { data } = await axios.get(`${BASE_URL}${params}`);

    // Respond to this request with data from NASA api
    cachedData = data;
    cacheTime = Date.now();
    data.cacheTime = cacheTime;

    return res.json(data);
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
